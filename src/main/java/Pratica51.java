import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51  {

    public static void main(String[] args) throws MatrizInvalidaException{
        Matriz orig = new Matriz(3, 2);
        Matriz matriz1 = new Matriz(3, 2);
        Matriz matriz2 = new Matriz(2, 3);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        double[][] m1 = matriz1.getMatriz();
        m1[0][0] = 5.0;
        m1[0][1] = 5.0;
        m1[1][0] = 5.0;
        m1[1][1] = 5.0;
        m1[2][0] = 5.0;
        m1[2][1] = 5.0;
        double[][] m2 = matriz2.getMatriz();
        m2[0][0] = 5.0;
        m2[0][1] = 5.0;
        m2[0][2] = 5.0;
        m2[1][0] = 5.0;
        m2[1][1] = 5.0;
        m2[1][2] = 5.0;
        try{
            orig.prod(matriz2);
            orig.soma(matriz1);
            orig.soma(matriz2);
            orig.prod(matriz1);
        }catch(SomaMatrizesIncompativeisException erro1){
            System.out.println("erro : " + erro1.getLocalizedMessage());
        }
        catch(ProdMatrizesIncompativeisException erro2){
            System.out.println("erro : " + erro2.getLocalizedMessage());
        }
        System.out.println(orig.prod(matriz2));
        System.out.println(orig.soma(matriz1));
    }
}
